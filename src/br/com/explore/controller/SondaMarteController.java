package br.com.explore.controller;
import java.util.Set;

import br.com.explore.model.EnumDirecao;
import br.com.explore.model.Planalto;
import br.com.explore.model.SondaMarte;

/**
 * 
 * @author Thiago B. Santos
 *
 */
public class SondaMarteController {
	
	private static Planalto planalto;
	
	public Set<SondaMarte> visualizarSondas() {
		return planalto.visualizarSondas();
	}
	
	public SondaMarteController() {
	}
	
	public void desenharPlanalto(int tamanhoX, int tamanhoY) {
		if (tamanhoX > 0 && tamanhoY > 0) {
			planalto = new Planalto(tamanhoX, tamanhoY);
		} else {
			throw new IllegalArgumentException("Os valores para as dimens�es devem ser maior do que zero.");
		}	
	}	

	public void orientarSonda(int posicaoX, int posicaoY, String direcao, String orientacoes) throws Exception {
		
		EnumDirecao enumDirecao = EnumDirecao.NORTE;
		
		if (posicaoX < 0 || posicaoY < 0) {
			throw new Exception("Posi��o inv�lida. Dever ser maior do que zero.");
		}
		
		if(direcao.equals("N")) enumDirecao = EnumDirecao.NORTE;
		if(direcao.equals("S")) enumDirecao = EnumDirecao.SUL;
		if(direcao.equals("E")) enumDirecao = EnumDirecao.LESTE;
		if(direcao.equals("W")) enumDirecao = EnumDirecao.OESTE;
		
		SondaMarte sonda = planalto.buscarSonda(posicaoX, posicaoY);
		
		//Adiciona somente se j� n�o houver sonda nesta posi��o
		if (sonda == null) {
			sonda = new SondaMarte(posicaoX, posicaoY, enumDirecao);
			sonda.setPlanalto(planalto);
			planalto.add(sonda);
		}
		
		sonda.orientacao(orientacoes);
	}
	
	/*
	public static void main(String args[]) {
    	SondaMarte sonda = new SondaMarte();
    	
    	sonda.ajustarPosicao(1, 2, EnumDirecao.NORTE);
    	sonda.orientacao("LMLMLMLMM");
    	sonda.exibirPosicao();
    	
    	sonda.ajustarPosicao(3, 3,  EnumDirecao.LESTE);
    	sonda.orientacao("MMRMMRMRRM");
    	sonda.exibirPosicao();
    }*/
}
