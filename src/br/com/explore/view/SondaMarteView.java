package br.com.explore.view;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.sun.jersey.api.client.ClientResponse.Status;

import br.com.explore.controller.SondaMarteController;
import br.com.explore.model.SondaMarte;

@Path("/")
@Consumes("application/json")
@Produces("application/json; charset=UTF-8")

public class SondaMarteView {
	
	private SondaMarteController controller = new SondaMarteController();
	
	@Path("/desenharplanalto")
	@GET
	public Response desenharPlanalto(@QueryParam("tamanhoX") int tamanhoX, @QueryParam("tamanhoY") int tamanhoY) {
		if (tamanhoX <= 0 || tamanhoY <= 0) {
			return Response.status(Status.BAD_REQUEST).entity("Par�metros para cria��o do planalto inv�lidos, devem ser maior do que zero: " + tamanhoX + " " + tamanhoY).build();
		}
		
		controller.desenharPlanalto(tamanhoX, tamanhoY);	
		return Response.status(200).build();
	}
	
	@Path("/orientarsonda/{posicaoX}/{posicaoY}")
	@POST
	public Response orientarSonda(@PathParam("posicaoX") int posicaoX, @PathParam("posicaoY") int posicaoY,
			@QueryParam("direcao") String direcao, @QueryParam("orientacoes") String orientacoes) throws Exception {
		
		if (posicaoX >= 0 && posicaoY >= 0 && direcao.matches("[NSEW]") && orientacoes.matches("[LRM]+")) {
			
			controller.orientarSonda(posicaoX, posicaoY, direcao, orientacoes);
			return Response.status(200).build();
			
		} else {
			StringBuilder msgErro = new StringBuilder("Instru��es de comando e direcionamento inv�lidos: ");
			
			msgErro.append("posicaoX=").append(posicaoX).append(",")
						.append("posicaoY=").append(posicaoY).append(",")
						.append("direcao=").append(direcao).append(",")
						.append("orientacoes=").append(orientacoes);
			
			return Response.status(Status.BAD_REQUEST).entity(msgErro).build();
		}
	}
		
	@Path("/visualizarsondas")
	@GET
	public String visualizarSondas() {
		JSONObject retornoJson = new JSONObject();
				
		try {
			Set<SondaMarte> sondas = controller.visualizarSondas();
			retornoJson.put("sondas", sondas.toString());
			
		} catch (Exception ex) {}
		
		return retornoJson.toString();
	}
	
}
