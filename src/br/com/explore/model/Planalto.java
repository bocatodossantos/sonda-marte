package br.com.explore.model;


import java.util.LinkedHashSet;
import java.util.Set;

public class Planalto {
	
	public int getTamanhoX() {
		return tamanhoX;
	}

	public void setTamanhoX(int tamanhoX) {
		this.tamanhoX = tamanhoX;
	}

	public int getTamanhoY() {
		return tamanhoY;
	}

	public void setTamanhoY(int tamanhoY) {
		this.tamanhoY = tamanhoY;
	}

	private int tamanhoX;	
	private int tamanhoY;
	
	//Utilizado LinkdedHashSet para manter a ordem de inser��o e n�o permitir duplicatas
	private final Set<SondaMarte> sondas = new LinkedHashSet<SondaMarte>();
	
	public Planalto(int tamanhoX, int tamanhoY) {
		if (tamanhoX <= 0 || tamanhoY <= 0) {
			throw new IllegalArgumentException("Tamanho informado inv�lido: [" + tamanhoX + "," + tamanhoY + "]");
		}
		this.tamanhoX = tamanhoX;
		this.tamanhoY = tamanhoY;
	}

	public boolean add(SondaMarte sonda) throws Exception {
		
		if (posicaoValida(sonda.getCoordenadas().getX(), sonda.getCoordenadas().getY())) {
			if (posicaoDisponivel(sonda.getCoordenadas().getX(), sonda.getCoordenadas().getY())) {
				sondas.add(sonda);
				
				return true;
			} else {
				return false;
			}
		} else {
			throw new Exception("INV�LIDO: Posicionado fora do planalto");
		}
	}
	
	public boolean posicaoValida(int posicaoX, int posicaoY) {
		return posicaoX <= tamanhoX && posicaoY <= tamanhoY;
	}
	
	public boolean posicaoDisponivel(int posicaoX, int posicaoY) {
		return !sondas.contains(new SondaMarte(posicaoX, posicaoY));
	}
	
	public Set<SondaMarte> visualizarSondas() {
		return new LinkedHashSet<SondaMarte>(sondas);
	}

	public SondaMarte buscarSonda(int posicaoX, int posicaoY) {
		for(SondaMarte sonda: sondas) {
			
			if(sonda.getCoordenadas().getX() == posicaoX && sonda.getCoordenadas().getY() == posicaoY) {
				return sonda;
			}
		}
		
		return null;
	}

}
