package br.com.explore.model;

public class SondaMarte {
	
    private Coordenadas coordenadas;
    private Planalto planalto;
    
    public Coordenadas getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}

	private EnumDirecao direcao = EnumDirecao.NORTE;
		
	public SondaMarte(int posicaoX, int posicaoY, EnumDirecao direcao) {
		coordenadas = new Coordenadas();
		
		this.coordenadas.setX(posicaoX);
		this.coordenadas.setY(posicaoY);
		this.direcao = direcao;
	}
	
	public SondaMarte(int posicaoX, int posicaoY) {
		this(posicaoX, posicaoY, EnumDirecao.NORTE);
	}
    
	@Override
    public String toString() {
        String direcaoPrint = "N";
        
        if (direcao == EnumDirecao.NORTE) {
        	direcaoPrint = "N";
        } else if (direcao == EnumDirecao.LESTE) {
        	direcaoPrint = "E";
        } else if (direcao == EnumDirecao.SUL) {
        	direcaoPrint = "S";
        } else if (direcao == EnumDirecao.OESTE) {
        	direcaoPrint = "W";
        }
        
        return	coordenadas.getX().toString() + " " +  
        		coordenadas.getY().toString() +  " " +  
        					direcaoPrint;
    }
    
    public void moverSonda() {
    	coordenadas.mover(direcao, planalto);    	    
    }
    
    public void orientacao(String orientacoes) {
    	char orientacao;
    	
        for (int i = 0; i < orientacoes.length(); i++ ) {
        	
        	orientacao = orientacoes.charAt(i);
        	
        	if (orientacao == 'L') {
        		virarEsquerda();
        		
            } else if (orientacao == 'R') {
            	virarDireita();
            	
            } else if (orientacao == 'M') {
            	
                moverSonda();
            } else {
                throw new IllegalArgumentException(
                        "Comando inv�lido!");
            }
        }
    }    
        
    
    public void virarEsquerda() {
    	switch(direcao) {
    		case NORTE:
    			direcao = EnumDirecao.OESTE;
    			break;
    		case LESTE:
    			direcao = EnumDirecao.NORTE;
    			break;
    		case SUL:
    			direcao = EnumDirecao.LESTE;
    			break;
    		case OESTE:
    			direcao = EnumDirecao.SUL;
    			break;
    	}
    }
    
    public void virarDireita() {
	    switch(direcao) {
			case NORTE:
				direcao = EnumDirecao.LESTE;
				break;
			case LESTE:
				direcao = EnumDirecao.SUL;
				break;
			case SUL:
				direcao = EnumDirecao.OESTE;
				break;
			case OESTE:
				direcao = EnumDirecao.NORTE;
				break;
		}
    }
    
    public void setPlanalto(Planalto planalto) {
		this.planalto = planalto;
	}

}
