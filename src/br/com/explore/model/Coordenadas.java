package br.com.explore.model;

public class Coordenadas {
	
	private Integer x;
	private Integer y;
	
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	
	public void mover(EnumDirecao direcao, Planalto planalto) {
		
		switch(direcao) {
			case NORTE:
				if (planalto != null && planalto.posicaoValida(x, (y + 1)) && (y + 1) <= planalto.getTamanhoY()) {
					y++;
				}							
				break;
				
			case LESTE:
				if (planalto != null && planalto.posicaoValida((x + 1), y) && (x + 1) <= planalto.getTamanhoX()) {
					x++;
				}
				break;
			case SUL:
				if (planalto != null && planalto.posicaoValida(x, (y - 1)) && (y - 1) >= 0) {
					y--;
				}
				break;
				
			case OESTE:
				if (planalto != null && planalto.posicaoValida((x - 1), y) && (x - 1) >= 0) {
					x--;
				}
				break;
		}
	}
}
