package br.com.explore.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.explore.controller.SondaMarteController;
import br.com.explore.model.EnumDirecao;
import br.com.explore.model.SondaMarte;
import br.com.explore.view.SondaMarteView;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class SondaMarteViewTest {
	
	@Tested
	private SondaMarteView sondaMarteView;
	
	@Mocked
	private SondaMarteController sondaMarteController;
	
	@Test
	public void testDefinirPlanaltoComParametroInvalido() {
		Response response = sondaMarteView.desenharPlanalto(0, 0);
		assertEquals(400, response.getStatus()); response.getStatus();
		
		response = sondaMarteView.desenharPlanalto(-1, -1);
		assertEquals(400, response.getStatus()); response.getStatus();
	}
	
	@Test
	public void testDefinirPlanaltoSucesso() {
		new Expectations() {{
			sondaMarteController.desenharPlanalto(anyInt, anyInt); minTimes = 1;
		}};
		Response response = sondaMarteView.desenharPlanalto(5, 5);
		assertEquals(200, response.getStatus()); response.getStatus();
	}
	
	@Test
	public void testControlarSondaComPosicaoInvalido() throws Exception {
		Response response = sondaMarteView.orientarSonda(-1, 0, "N", "LM");
		assertEquals(400, response.getStatus()); response.getStatus();
		
		response = sondaMarteView.orientarSonda(0, -1, "N", "LM");
		assertEquals(400, response.getStatus()); response.getStatus();
	}
	
	@Test
	public void testControlarSondaComDirecaoInvalida() throws Exception {
		Response response = sondaMarteView.orientarSonda(0, 0, "Z", "LM");
		assertEquals(400, response.getStatus()); response.getStatus();
		
		response = sondaMarteView.orientarSonda(0, 0, "A", "LM");
		assertEquals(400, response.getStatus()); response.getStatus();
	}
	
	@Test
	public void testControlarSondaSucesso() throws Exception {
		new Expectations() {{
			sondaMarteController.orientarSonda(anyInt, anyInt, "N", "LMRLMRLMRLMM"); minTimes = 1;
		}};
		
		Response response = sondaMarteView.orientarSonda(0, 0, "N", "LMRLMRLMRLMM");
		assertEquals(200, response.getStatus()); response.getStatus();
	}
	
	@Test
	public void testControlarSondaComComandosInvalidos() throws Exception {
		Response response = sondaMarteView.orientarSonda(0, 0, "N", "AB");
		assertEquals(400, response.getStatus()); response.getStatus();
		
		response = sondaMarteView.orientarSonda(0, 0, "N", "XZ");
		assertEquals(400, response.getStatus()); response.getStatus();
	}
	
	@Test
	public void testListarSonda() {
		Set<SondaMarte> sondas = new HashSet<SondaMarte>();
		sondas.add(new SondaMarte(1, 2, EnumDirecao.LESTE));
		
		new Expectations() {{
			sondaMarteController.visualizarSondas(); result = sondas; minTimes = 1;
		}};

		JSONObject jsonObject = new JSONObject(sondaMarteView.visualizarSondas());
		assertTrue(jsonObject.has("sondas"));
		
		assertEquals("[1 2 E]", jsonObject.getString("sondas"));
	}
}