package br.com.explore.test;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.explore.controller.SondaMarteController;
import br.com.explore.model.Planalto;
import br.com.explore.model.SondaMarte;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class SondaMarteControllerTest {
	
	@Tested
	private SondaMarteController sondaMarteController;
	
	@Injectable
	private Planalto planalto;
	
	@Test(expected = Exception.class)
	public void desenharPlanaltoComParametrosNegativos() {
		sondaMarteController.desenharPlanalto(-1, -1);
	}
	
	@Test(expected = Exception.class)
	public void desenharPlanaltoComParametrosZerados() {
		sondaMarteController.desenharPlanalto(0, 0);
	}
	
	@Test
	public void testDesenharPlanalto(@Mocked Planalto planalto) {
		new Expectations() {{
			new Planalto(1, 1); minTimes = 1;
		}};
		sondaMarteController.desenharPlanalto(1, 1);
	}
	
	@Test(expected = Exception.class)
	public void testOrientarSondaComPosicaoInvalida() throws Exception{
		sondaMarteController.orientarSonda(-1, -1, "N", "R");
	}
	
	@Test(expected = Exception.class)
	public void testOrientarSondaComDirecaoInvalida() throws Exception{
		sondaMarteController.orientarSonda(0, 0, "F", "R");
	}
	
	@Test(expected = Exception.class)
	public void testOrientarSondaComOrientacaoInvalido() throws Exception{
		sondaMarteController.orientarSonda(0, 0, "S", "LMLMLMV");
	}
}
